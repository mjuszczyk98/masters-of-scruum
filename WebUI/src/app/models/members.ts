export interface Members {
  SCRUM_MASTER: Array<string>;
  TEAM: Array<string>;
  PRODUCT_OWNER: Array<string>;
}

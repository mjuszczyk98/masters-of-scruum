export interface Meeting{
    id: number;
    sprintId: string;
    meetingName: string;
    members: string[];
    startDateTime: Date | string;
    endDateTime: Date | string;
}

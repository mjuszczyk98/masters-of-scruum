export interface Sprint{
    id: number;
    sprintName: string;
    goal: string;
    startDate: Date;
    endDate: Date;
}

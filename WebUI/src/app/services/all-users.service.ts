import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AllUsersService {
  private users: Observable<User[]>;
  constructor(private http: HttpClient) {}

  public load(): Observable<User[]>{
    this.users = this.http.get<User[]>('http://localhost:8081/getAllUsers');
    return this.users;
  }
  public get(): Observable<User[]>{
    return this.users;
  }
  public filter(prefix: string): Observable<User[]>{
    this.users = this.http.get<User[]>('http://localhost:8081/teams/users?emailRgx=' + prefix);
    return this.users;
  }
}

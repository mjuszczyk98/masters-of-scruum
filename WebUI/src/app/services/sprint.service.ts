import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Sprint } from '../models/sprint';

@Injectable({
  providedIn: 'root'
})
export class SprintService {
    private sprints: Observable<Sprint[]>;

    constructor(private http: HttpClient) { }

    public load(teamId: string): Observable<Sprint[]> {
        this.sprints = this.http.get<Sprint[]>(`http://localhost:8081/teams/${teamId}/sprints?startIndex=0&startIndexOffset=0`);
        return this.sprints;
    }

    public update(teamId: string, sprint: Sprint): Observable<Sprint> {
        return this.http.put<Sprint>(`http://localhost:8081/sprints/${sprint.id}`, sprint);
    }

    public create(teamId: string, sprint: Sprint): Observable<Sprint> {
        return this.http.post<Sprint>(`http://localhost:8081/teams/${teamId}/sprints`, sprint);
    }

    public remove(teamId: string, sprint: Sprint): Observable<Sprint> {
        return this.http.delete<Sprint>(`http://localhost:8081/teams/${teamId}/sprints/${sprint.id}`);
    }

    public get(): Observable<Sprint[]> {
        return this.sprints;
    }
}

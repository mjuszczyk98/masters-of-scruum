import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { isUndefined } from 'util';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private user: Observable<User>;

  constructor(private http: HttpClient) { }

  public login(user: User): Observable<User>{
    this.user = this.http.get<User>('http://localhost:8081/loginUser?email=' + user.email + '&password=' + user.password);
    this.user.toPromise().then(r => console.log(r));
    return this.user;
  }

  public logout(): void{
    this.user = undefined;
  }

  public register(user: User): Observable<User> {
    this.user = this.http.post<User>('http://localhost:8081/RegistrationUser', user);
    this.user.toPromise().then(r => console.log(r));
    return this.user;
  }

  public logged(): string{
    let userLogin: string;
    if (this.user !== undefined){
      this.user.subscribe(u => userLogin = u.email);
    }
    return userLogin;
  }
}

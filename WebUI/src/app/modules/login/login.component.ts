import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class LoginComponent implements OnInit{
  public user: User;

  constructor(private loginService: LoginService){
    this.user = {id: '', firstName: '', lastName: '', role: '', email: '', password: ''};
  }

  onSubmit() {
    this.loginService.login(this.user);
  }

  ngOnInit() {
    if (! this.loginService.logged()){
      // TODO redirect to home
      return;
    }
  }
}

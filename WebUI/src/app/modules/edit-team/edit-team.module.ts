import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditTeamComponent } from './edit-team.component';



@NgModule({
  declarations: [
    EditTeamComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FormsModule
  ]
})
export class EditTeamModule { }

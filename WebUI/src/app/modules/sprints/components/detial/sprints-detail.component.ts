import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Sprint } from 'src/app/models/sprint';

@Component({
  selector: 'app-sprints-details-component',
  templateUrl: './sprints-detail.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
    .margin {margin-left: 50px; margin-right: 50px;}
  `]
})
export class SprintsDetailComponent{
  @Input() sprint: Sprint;
  @Output() save: EventEmitter<Sprint> = new EventEmitter();

  public selectedSprint: Sprint;

  public onSubmit(): void{
    this.save.emit(this.sprint);
  }
}

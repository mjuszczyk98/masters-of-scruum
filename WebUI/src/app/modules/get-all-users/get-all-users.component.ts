import { Component, OnInit } from '@angular/core';
import { AllUsersService } from '../../services/all-users.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-get-all-users',
  templateUrl: './get-all-users.component.html',
})
export class GetAllUsersComponent implements OnInit{
  public test: User[];

  constructor(private allUsersService: AllUsersService){}

  ngOnInit(){
    this.allUsersService.get().subscribe((ret) => {
      this.test = ret;
    });
  }
}

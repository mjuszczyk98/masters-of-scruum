import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route, ActivatedRoute, Router, RouterModule } from '@angular/router';
import { tap, map } from 'rxjs/operators';
import { Team } from 'src/app/models/team';
import { CreateTeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-teams-container',
  templateUrl: './teams-container.component.html',
  styles: [`
    .all-screen { width: 100%; height: 100%; }
    .margin{margin: 25px;}
  `]
})
export class TeamsComponent implements OnInit{
  public selectedTeam: Team;
  public teams: Team[];
  private userId: string;

  constructor(private teamService: CreateTeamService, private route: ActivatedRoute, private router: Router){
    this.teams = [] as Team[];
    this.selectedTeam = {} as Team;
  }

  ngOnInit() {
    this.userId = this.route.snapshot.paramMap.get('id');
    this.teamService.getMyTeams(this.userId).subscribe((result: Team[]) => {
      this.teams = result;
    });
  }

  edit() {
    if (this.selectedTeam !== null){
      this.router.navigate(['/editTeam' , this.selectedTeam.id]);
    }
  }

  async leave() {
    // tutaj opuszczanie zespolu
  }

  selected(team: Team): void{
    this.selectedTeam = team;
  }

  async save(team: Team) {
    this.teamService.createTeam(team);
  }

  async refresh() {
    // refresh
  }
}

import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Meeting } from 'src/app/models/meeting';
import { ListViewComponent } from '@syncfusion/ej2-angular-lists';

@Component({
  selector: 'app-meetings-details-component',
  templateUrl: './meetings-detail.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class MeetingsDetailComponent implements OnChanges {
  @Input() meeting: Meeting;
  @Output() save: EventEmitter<Meeting> = new EventEmitter();
  @ViewChild('list', {static: true}) public list: ListViewComponent;

  public bool: boolean[] = [true, false, false];

  public data: string[] = ['SCRUM_MASTER', 'PRODUCT_OWNER', 'TEAM'];

  public selectedMeeting: Meeting;

  public onSubmit(): void{
    let m: string[] = [] as string[];
    if (this.bool[0]) {
      m = [...m, 'SCRUM_MASTER'];
    }
    if (this.bool[1]) {
      m = [...m, 'PRODUCT_OWNER'];
    }
    if (this.bool[2]) {
      m = [...m, 'TEAM'];
    }
    this.meeting.members = m;
    this.bool = [false, false, false] as boolean[];
    this.save.emit(this.meeting);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.meeting) {
      this.bool = [false, false, false] as boolean[];
      for (const s of this.meeting.members ? this.meeting.members : [] as string[]) {
        if (s === 'SCRUM_MASTER') {
          this.bool[0] = true;
        } else if (s === 'PRODUCT_OWNER') {
          this.bool[1] = true;
        } else if (s === 'TEAM') {
          this.bool[2] = true;
        }
      }
    }
  }
}

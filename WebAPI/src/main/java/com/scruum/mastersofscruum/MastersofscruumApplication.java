package com.scruum.mastersofscruum;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@OpenAPIDefinition
public class MastersofscruumApplication {

    public static void main(String[] args) {
        SpringApplication.run(MastersofscruumApplication.class, args);
    }

}

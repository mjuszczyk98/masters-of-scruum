package com.scruum.mastersofscruum.mapper;

import com.scruum.mastersofscruum.dto.TeamDto;
import com.scruum.mastersofscruum.model.Team;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TeamMapper {

    TeamMapper INSTANCE = Mappers.getMapper(TeamMapper.class);

    TeamDto toDto(Team team);

    Team fromDto(TeamDto teamDto);

    List<TeamDto> toDtoList(List<Team> teamList);
}
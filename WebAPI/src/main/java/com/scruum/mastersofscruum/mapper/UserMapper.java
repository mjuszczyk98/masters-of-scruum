package com.scruum.mastersofscruum.mapper;


import com.scruum.mastersofscruum.dto.UserDto;
import com.scruum.mastersofscruum.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto toDto(User user);

    User fromDto(UserDto userDto);


}

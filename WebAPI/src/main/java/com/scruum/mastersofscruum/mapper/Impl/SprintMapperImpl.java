package com.scruum.mastersofscruum.mapper.Impl;

import com.scruum.mastersofscruum.dto.SprintDto;
import com.scruum.mastersofscruum.mapper.SprintMapper;
import com.scruum.mastersofscruum.model.Sprint;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SprintMapperImpl implements SprintMapper {
    @Override
    public SprintDto toDto(Sprint sprint) {
        return SprintMapper.INSTANCE.toDto(sprint);
    }

    @Override
    public Sprint fromDto(SprintDto sprintDto) {
        return SprintMapper.INSTANCE.fromDto(sprintDto);
    }

    @Override
    public List<SprintDto> toDtoList(List<Sprint> sprintList) {
        return SprintMapper.INSTANCE.toDtoList(sprintList);
    }
}

package com.scruum.mastersofscruum.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Data
public class Sprint {
    @Id
    private String id;
    private String sprintName;
    private String goal;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Schema(pattern = "yyyy-MM-dd")
    private LocalDate startDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Schema(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    public Sprint() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSprintName() {
        return sprintName;
    }

    public void setSprintName(String sprintName) {
        this.sprintName = sprintName;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Sprint(String id, String sprintName, String goal, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.sprintName = sprintName;
        this.goal = goal;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}

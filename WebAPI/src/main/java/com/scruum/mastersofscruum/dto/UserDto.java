package com.scruum.mastersofscruum.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class UserDto {

    public String id;

    @NotNull
    public String firstName;

    @NotNull
    public String lastName;

    @Email
    public String email;

    public UserDto() {
    }

    public UserDto(String id, @NotNull String firstName, @NotNull String lastName, @Email String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

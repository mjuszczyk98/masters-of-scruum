package com.scruum.mastersofscruum.dto;

import com.scruum.mastersofscruum.model.Role;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;


public class TeamDto {
    public String id;
    @NotNull
    public String teamName;
    @NotNull
    public Map<Role, List<String>> members;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Map<Role, List<String>> getMembers() {
        return members;
    }

    public void setMembers(Map<Role, List<String>> members) {
        this.members = members;
    }

    public TeamDto() {
    }

    public TeamDto(String id, @NotNull String teamName, @NotNull Map<Role, List<String>> members) {
        this.id = id;
        this.teamName = teamName;
        this.members = members;
    }
}

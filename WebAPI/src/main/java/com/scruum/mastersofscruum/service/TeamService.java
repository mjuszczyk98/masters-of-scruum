package com.scruum.mastersofscruum.service;

import com.scruum.mastersofscruum.dto.TeamDto;
import com.scruum.mastersofscruum.model.Team;
import com.scruum.mastersofscruum.repository.TeamRepository;

public interface TeamService {

    public TeamRepository teamRepository();

    public Team updateTeam(Team team, TeamDto teamDto);

    Team findById(String teamId);

}

package com.scruum.mastersofscruum.service.Impl;

import com.scruum.mastersofscruum.dto.TeamDto;
import com.scruum.mastersofscruum.model.Team;
import com.scruum.mastersofscruum.repository.TeamRepository;
import com.scruum.mastersofscruum.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class TeamServiceImpl implements TeamService {

    private final TeamRepository teamRepository;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, MongoTemplate mongoTemplate) {
        this.teamRepository = teamRepository;
        this.mongoTemplate = mongoTemplate;
    }

    public TeamRepository teamRepository() {
        return teamRepository;
    }

    @Override
    public Team updateTeam(Team team, TeamDto teamDto) {

        team.setTeamName(teamDto.getTeamName());
        team.setMembers(teamDto.getMembers());
        team = teamRepository.save(team);
        return team;
    }

    @Override
    public Team findById(String teamId) throws ResponseStatusException {
        Optional<Team> optionalTeam = teamRepository.findById(teamId);
        if (optionalTeam.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return optionalTeam.get();
    }



}

package com.scruum.mastersofscruum.repository;

import com.scruum.mastersofscruum.model.Sprint;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SprintRepository extends MongoRepository<Sprint, String> {



}

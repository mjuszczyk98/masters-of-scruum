package com.scruum.mastersofscruum.repository;

import com.scruum.mastersofscruum.model.ERole;
import com.scruum.mastersofscruum.model.Roles;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository  extends MongoRepository<Roles, String> {
    Optional<Roles> findByName(ERole name);
}
